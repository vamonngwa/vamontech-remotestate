
# s3 
resource "aws_s3_bucket" "vamonstatebucket" {
  bucket = "vamonstatebucket"   # create variabl and   count 
}

resource "aws_s3_bucket_acl" "vamonstatebucket" {
  bucket = aws_s3_bucket.vamonstatebucket.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "versioning_vamonstatebucket" {
  bucket = aws_s3_bucket.vamonstatebucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_dynamodb_table" "terraform-state-lock" {
  name           = "terraform-state-lock"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID"
 

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name        = "terraform-state-lock"
    Environment = "Dev"
  }
}